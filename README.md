# mine_sweeping
 
"mine_sweeping"は、pythonで動く、自作マインスイーパです。
 
# Requirement

* python 3.7.2
* pygame 2.0.0
 
# Installation
 
```bash
pip install pygame
```
 
# Usage
 
```bash
git clone https://gitlab.com/my_practice1/my-works
cd mine_sweeping
python3 mine_sweeping.py
```

# How to play

## タイトル画面
ゲームを始める：Enter

## ゲームプレイ中
マスを開く：左クリック
目印を置く：右クリック
リスタート：SPACE

## ゲームオーバー時
リトライ：ESC

## いつでも
ゲームをやめる：バツボタンを押す

# Note
 
動作確認済みOS
macOS Catalina 10.15.5

macのトラックパッドで右クリックする場合は、Ctrl+クリックではうまくいかないので、
トラックパッドを２本指でクリックして下さい。
 
# Author
 
* Riku Gondo
* Keio University
* riku_gondow@keio.jp
 
# License

BGM：魔王魂

Thank you for your time!
